'use strict';

chrome.runtime.onMessage.addListener(function(request, sender) {
  //trigger a redirect on the active tab
  chrome.tabs.query({currentWindow: true, active: true}, function (tabs) {
    var tab_id = tabs[0].id
    chrome.tabs.update(tab_id, {url: request.redirect})
  })
});
