'use strict';

var dtApp = angular.module(['dtApp'], [
  'dtControllers',
  'dtServices',
  'ui.router',
  'ngAnimate',
  'ngSanitize',
  'ngTouch',
  'ui.bootstrap'
])
.run([
  '$q', '$rootScope','$state','$stateParams',
  function($q, $rootScope, $state, $stateParams) {
    // Set defaults
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
  }
])
.config([
  '$stateProvider','$urlRouterProvider','$locationProvider',
  function($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider
    .otherwise('/');

    $stateProvider
    .state('body', {
      abstract: true,
      url: '',
      templateUrl: 'partials/body.html',
      controller: 'NavController'
    })
    .state('index', {
      url:'/',
      templateUrl: 'partials/index.html',
      parent: 'body',
      controller: 'TwitterController'
    })

    $locationProvider.html5Mode(true);
  }
]);
