Twitterpated
=============

Using DTRelay to Build a Chrome Extension
-----------------------------------------

### Set up the extension

1. Create a folder called _extension/_.

**extension/bower.json**
```
{
    "name":  "example-site",
    "version": "1.0.0",
    "dependencies": {
      "jquery": "latest",
      "jquery-ui": "latest",
      "bootstrap": "latest",
      "angular": "latest",
      "angular-bootstrap": "latest",
      "angular-ui": "latest",
      "angular-touch": "latest",
      "angular-animate": "latest",
      "angular-resource": "latest",
      "angular-sanitize": "latest",
      "angular-ui-router": "latest",
      "deep-thought-relay-client": "latest"
    }
}
```

Install the bower packages. `bower install`



### Set up the relay

`git clone git@github.com:expressive-analytics/deep-thought.php-relay-server.git relay`

`composer install -d relay`

#### Initialize the database

**local/storage.json**
```
{
  "relay": {
    "connector": "DTSQLiteDatabase",
    "dsn": "file:///var/www/deepthought/storage/dt-development.db"
  }
}
```

Run the init SQL file. `sqlite3 storage/dt-development.db < relay/tests/init.sql`

#### Connect to Twitter APIs

**local/apis.json**
```
{
	"twitter":{
    "url":"https://api.twitter.com/1.1/",
    "delegates":[
			"DTOAuth1Delegate"
    ],
    "consumer_key":"********",
    "consumer_secret":"*********",
		"request_token_url":"https://api.twitter.com/oauth/request_token",
		"authorize_url":"https://api.twitter.com/oauth/authorize",
		"access_token_url":"https://api.twitter.com/oauth/access_token",
		"callback_url":"http://localhost:8887/default.php?api=twitter"
  }
}
```

**local/config.json**
```
{
  "logs":{
    "error_log": "/var/www/deepthought/logs/error.log",
    "debug_log": "/var/www/deepthought/logs/error.log",
    "info_log": "/var/www/deepthought/logs/error.log",
    "permissions": "0775"
  },
  "base_url":"http://localhost:8000"
}
```

### Start your engines

**docker-compose.yml**
```
relay:
 ports:
  - "8887:80"
 volumes:
  - .:/var/www/deepthought/
 image: expressiveanalytics/dt-development.php
 hostname: relay.local
```


### Debugging your extensions



#### Resources
Getting Started: https://developer.chrome.com/extensions/getstarted
Sample Repository:

Chrome Extension Security Policy: https://developer.chrome.com/extensions/contentSecurityPolicy
